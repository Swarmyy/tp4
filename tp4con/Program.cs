﻿using System;

namespace tp4con
{
    class Program
    {
        public static uint areaSquare(uint side){
            return side*side;
        }
        public static uint areaRectangle(uint length,uint width){
            return length*width;
        }
        public static uint areaTriangle(uint baseT,uint height){
            return baseT*height/2;
        }
        static void Main(string[] args)
        {
            
        }
    }
}
