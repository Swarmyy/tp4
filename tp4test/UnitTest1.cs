using System;
using Xunit;

using tp4con;

namespace tp4test
{
    public class UnitTest1
    {
        [Fact]
        public void SqArea()
        {
            Assert.Equal(4,Program.areaSquare(2));
        }
        [Fact]
        public void RectangleArea()
        {
            Assert.Equal(8,Program.areaRectangle(2,4));
        }
        [Fact]
        public void TriangleArea()
        {
            Assert.Equal(4,Program.areaTriangle(2,4));
        }
    }
}
